package com.gitlab.kevinnowak.slidingwindow;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MaximumAverageSubarray1Test {
    @Test
    public void testFindMaxAverage() {
        assertEquals(12.75000, MaximumAverageSubarray1.findMaxAverage(new int[]{1, 12, -5, -6, 50, 3}, 4));
        assertEquals(5.00000, MaximumAverageSubarray1.findMaxAverage(new int[]{5}, 1));
    }
}
