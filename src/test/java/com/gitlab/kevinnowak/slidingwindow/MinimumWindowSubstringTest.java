package com.gitlab.kevinnowak.slidingwindow;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MinimumWindowSubstringTest {
    @Test
    public void testMinWindow() {
        assertEquals("BANC", MinimumWindowSubstring.minWindow("ADOBECODEBANC", "ABC"));
        assertEquals("a", MinimumWindowSubstring.minWindow("a", "a"));
        assertEquals("", MinimumWindowSubstring.minWindow("a", "aa"));
    }
}
