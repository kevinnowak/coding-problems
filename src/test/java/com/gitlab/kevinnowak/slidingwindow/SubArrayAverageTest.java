package com.gitlab.kevinnowak.slidingwindow;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;

public class SubArrayAverageTest {
    @Test
    public void testNaive() {
        assertEquals(
                Arrays.toString(new double[]{2.0, 3.0, 4.0}),
                Arrays.toString(SubArrayAverage.naive(new int[]{1, 2, 3, 4, 5}, 3))
        );
        assertEquals(
                Arrays.toString(new double[]{2.2, 2.8, 2.4, 3.6, 2.8}),
                Arrays.toString(SubArrayAverage.naive(new int[]{1, 3, 2, 6, -1, 4, 1, 8, 2}, 5))
        );
    }

    @Test
    public void testSlidingWindow() {
        assertEquals(
                Arrays.toString(new double[]{2.0, 3.0, 4.0}),
                Arrays.toString(SubArrayAverage.slidingWindow(new int[]{1, 2, 3, 4, 5}, 3))
        );
        assertEquals(
                Arrays.toString(new double[]{2.2, 2.8, 2.4, 3.6, 2.8}),
                Arrays.toString(SubArrayAverage.slidingWindow(new int[]{1, 3, 2, 6, -1, 4, 1, 8, 2}, 5))
        );
    }
}
