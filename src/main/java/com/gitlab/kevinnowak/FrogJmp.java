package com.gitlab.kevinnowak;

public class FrogJmp {

    public static int solution(int X, int Y, int D) {
        return (int) Math.ceil(((double) (Y - X)) / D);
    }

    public static void main(String[] args) {
        System.out.println(solution(10, 75, 30));
    }
}
