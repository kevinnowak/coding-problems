package com.gitlab.kevinnowak.slidingwindow;

class SubArrayAverage {
    static double[] naive(int[] arr, int k) {
        double[] averages = new double[k];

        for (int i = 0; i <= arr.length - k; i++) {
            double sum = 0;

            for (int j = 0; j < k; j++) {
                sum += arr[i + j];
            }

            averages[i] = sum / k;
        }


        return averages;
    }

    static double[] slidingWindow(int[] arr, int k) {
        double[] averages = new double[k];
        int windowStart = 0;
        double windowSum = 0;
        int insertIndex = 0;

        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            windowSum += arr[windowEnd];

            if (windowEnd >= k - 1) {
                averages[insertIndex++] = windowSum / k;
                windowSum -= arr[windowStart++];
            }
        }

        return averages;
    }
}
