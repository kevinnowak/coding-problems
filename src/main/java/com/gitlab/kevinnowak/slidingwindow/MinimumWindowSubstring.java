package com.gitlab.kevinnowak.slidingwindow;

import java.util.HashMap;
import java.util.Map;

class MinimumWindowSubstring {
    static String minWindow(String s, String t) {
        if (t.isEmpty()) {
            return "";
        }

        Map<Character, Integer> tMap = new HashMap<>();

        for (char c : t.toCharArray()) {
            if (tMap.containsKey(c)) {
                tMap.put(c, tMap.get(c) + 1);
            } else {
                tMap.put(c, 1);
            }
        }

        int have = 0;
        int need = tMap.size();
        Map<Character, Integer> window = new HashMap<>();
        int[] bestSolution = new int[]{-1, -1};
        int bestSolutionLength = Integer.MAX_VALUE;
        int left = 0;

        for (int right = 0; right < s.length(); right++) {
            char c = s.charAt(right);

            if (window.containsKey(c)) {
                window.put(c, window.get(c) + 1);
            } else {
                window.put(c, 1);
            }

            if (tMap.containsKey(c) && window.get(c).equals(tMap.get(c))) {
                have++;
            }

            while (have == need) {
                if ((right - left + 1) < bestSolutionLength) {
                    bestSolution[0] = left;
                    bestSolution[1] = right;
                    bestSolutionLength = right - left + 1;
                }

                char charToBeRemoved = s.charAt(left);

                window.put(charToBeRemoved, window.get(charToBeRemoved) - 1);

                if (tMap.containsKey(charToBeRemoved) && window.get(charToBeRemoved) < tMap.get(charToBeRemoved)) {
                    have--;
                }

                left++;
            }
        }

        return bestSolution[0] == -1 ? "" : s.substring(bestSolution[0], bestSolution[1] + 1);
    }
}
