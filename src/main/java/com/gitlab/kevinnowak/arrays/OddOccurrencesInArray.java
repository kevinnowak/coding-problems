package com.gitlab.kevinnowak.arrays;

import java.util.HashSet;
import java.util.Set;

public class OddOccurrencesInArray {

    public static int solutionBitwiseXOR(int[] A) {
        int result = 0;

        for (int i : A) {
            result = result ^ i;
        }

        return result;
    }

    public static int solutionSet(int[] A) {
        Set<Integer> set = new HashSet<>();

        for (int i : A) {
            if (set.contains(i)) {
                set.remove(i);
            } else {
                set.add(i);
            }
        }

        return set.iterator().next();
    }

    public static void main(String[] args) {
        System.out.println(solutionBitwiseXOR(new int[]{9, 3, 9, 3, 9, 7, 9}));
        System.out.println(solutionSet(new int[]{9, 3, 9, 3, 9, 7, 9}));
    }
}
